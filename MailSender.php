<?php

/**
 * Mail Sender
 *
 * Klasa do obsługi wysyłania wiadomości e-mail
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @author Mariusz (Sephiro) Fornal <sephiro.boo.pl>
 * @link https://bitbucket.org/sephiro/mail-sender
 * @license http://opensource.org/licenses/gpl-3.0.html GNU Public License v3.0
 * @copyright Copyright (c) 2012, Mariusz Fornal
 * @version 1.0.4
 */
class MailSender {

    /**
     * Kodowanie znaków wiadomości
     * @var string
     */
    protected $_charset;

    /**
     * Tablica przechowująca adres i nazwę nadawcy
     * @var array
     */
    protected $_from = null;

    /**
     * Tablica przechowująca odbiorców w formie tablic z adresem i nazwą
     * @var array
     */
    protected $_recipients = array();

    /**
     * Tablica przechowująca odbiorców kopii w formie tablic z adresem i nazwą
     * @var array
     */
    protected $_recipientsCc = array();

    /**
     * Tablica przechowująca odbiorców ukrytej kopii w formie tablic z adresem i nazwą
     * @var array
     */
    protected $_recipientsBcc = array();

    /**
     * Zawiera adres zwrotny
     * @var string
     */
    protected $_replyTo = null;

    /**
     * Czy treść wiadomości to kod HTML
     * @var boolean
     */
    protected $_html = false;

    /**
     * Temat wiadomości
     * @var string
     */
    protected $_subject = '(No subject)';

    /**
     * Treść wiadomości
     * @var string
     */
    protected $_body = '(No body)';

    /**
     * Tablica załączników
     * @var array
     */
    protected $_attachments = array();

    /**
     * Tablica załączników użytych wenątrz wiadomości HTML (inline)
     * @var array
     */
    protected $_attachmentsWithCid = array();

    /**
     * Tekst alternatywny dla programów nie obsługujących kodu HTML
     * @var string
     */
    protected $_alternativeBody = '';

    /**
     * Nowy obiekt wiadomości
     * @param string $charset
     */
    public function __construct($charset = 'utf-8') {
        $this->_charset = $charset;
    }

    /**
     * Ustawia nadawcę wiadomości
     * @param string $emailAddress Adres e-mail nadawcy
     * @param string $name Nazwa nadawcy
     * @throws MailSenderException
     */
    public function setFrom($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_from = array('addr' => $emailAddress, 'name' => $name ? $this->_useCharset($name) : $name);
    }

    /**
     * Ustawia adres zwrotny wiadomości
     * @param string $replyTo
     * @throws MailSenderException
     */
    public function setReplyTo($replyTo) {
        if (!filter_var($replyTo, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_replyTo = $replyTo;
    }

    /**
     * Ustawia pojedynczego odbiorcę (czyści listę odbiorców)
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function setRecipient($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipients = array();
        $this->addRecipient($emailAddress, $name);
    }

    /**
     * Dodaje odbiorcę do listy
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function addRecipient($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipients[] = array('addr' => $emailAddress, 'name' => $name ? $this->_useCharset($name) : $name);
    }

    /**
     * Ustawia pojedynczego odbiorcę kopii (czyści listę odbiorców kopii)
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function setRecipientCc($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipientsCc = array();
        $this->addRecipientCc($emailAddress, $name);
    }

    /**
     * Dodaje odbiorcę kopii do listy
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function addRecipientCc($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipientsCc[] = array('addr' => $emailAddress, 'name' => $name ? $this->_useCharset($name) : $name);
    }

    /**
     * Ustawia pojedynczego odbiorcę kopii ukrytej (czyści listę odbiorców kopii ukrytych)
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function setRecipientBcc($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipientsBcc = array();
        $this->addRecipientBcc($emailAddress, $name);
    }

    /**
     * Dodaje odbiorcę kopii ukrytej do listy
     * @param string $emailAddress Adres e-mail odbiorcy
     * @param string $name Nazwa odbiorcy
     * @throws MailSenderException
     */
    public function addRecipientBcc($emailAddress, $name = null) {
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL))
            throw new MailSenderException('Given e-mail address is not valid');

        $this->_recipientsBcc[] = array('addr' => $emailAddress, 'name' => $name ? $this->_useCharset($name) : $name);
    }

    /**
     * Ustawia czy wiadomość ma być traktowana jako kod HTML
     * @param boolean $isHtml
     */
    public function isHTML($isHtml = true) {
        $this->_html = $isHtml;
    }

    /**
     * Ustawia temat wiadomości
     * @param string $subject
     */
    public function setSubject($subject) {
        $this->_subject = $this->_useCharset($subject);
    }

    /**
     * Ustawia treść wiadomości
     * @param string $body
     */
    public function setBody($body) {
        $this->_body = wordwrap($body, 70);
    }

    /**
     * Ustawia tekst alternatywny dla programów nie obsługujących HTML
     * @param string $alternativeBody
     */
    public function setAlternativeBody($alternativeBody) {
        $this->_alternativeBody = wordwrap($alternativeBody, 70);
    }

    /**
     * Dodaje załącznik do wiadomości z podanej ścieżki do pliku
     * @param string $filePath Bezwględna ścieżka do pliku z załącznikiem
     * @param string $cid Identyfikator załącznika inline (zostawić puste dla zwykłych załączników)
     * @param string $filename Nazwa pliku jaka ma się pokazać w wiadomości (zamiast oryginalnej)
     * @throws MailSenderException
     */
    public function addAttachmentFromFile($filePath, $cid = null, $filename = null) {
        if (!file_exists($filePath))
            throw new MailSenderException('Attachment file does not exists in path: ' . $filePath);

        $content = file_get_contents($filePath);
        $mimeType = $this->_getMimeType($filePath);
        $this->addAttachment($content, $filename ? $filename : basename($filePath), $mimeType, $cid);
    }

    /**
     * Dodaje załącznik do wiadomości wg podanych parametrów
     * @param string $content Treść załącznika (w czystej formie)
     * @param string $filename Nazwa pliku jaka wyświetli się w wiadomości
     * @param string $mimeType Typ mime pliku
     * @param type $cid Identyfikator załącznika inline (zostawić puste dla zwykłych załączników)
     */
    public function addAttachment($content, $filename, $mimeType = null, $cid = null) {
        $content = chunk_split(base64_encode($content));

        if(!$mimeType) $mimeType = $this->_getMimeType($filename);

        $att = array(
            'filename' => $filename ? $filename : 'attachment' . count($this->_attachments),
            'mime' => $mimeType,
            'content' => $content,
            'cid' => $cid
        );

        if ($cid) {
            $this->_attachmentsWithCid[] = $att;
        } else {
            $this->_attachments[] = $att;
        }
    }

    /**
     * Wysyła wcześniej skonfigurowaną wiadomość
     * @throws MailSenderException
     */
    public function send() {
        if (!$this->_from || !count($this->_recipients))
            throw new MailSenderException('Message sender or recipients not specified');

        $fromString = isset($this->_from['name']) ? $this->_from['name'] . ' <' . $this->_from['addr'] . '>' : $this->_from['addr'];
        $replyToString = $this->_replyTo ? $this->_replyTo : $this->_from['addr'];

        if (count($this->_attachments)) {
            $type = 'mixed';
        } elseif ($this->_html && $this->_alternativeBody) {
            $type = 'alt';
        } elseif ($this->_html && count($this->_attachmentsWithCid)) {
            $type = 'related';
        } else {
            $type = 'simple';
        }

        if ($type != 'simple')
            $boundary = md5(uniqid());

        $headers = "MIME-Version: 1.0\r\n";
        $headers.= "From: " . $fromString . "\r\n";
        $headers.= "Reply-To: " . $replyToString . "\r\n";

        switch ($type) {
            case 'simple':
                $headers.= "Content-type: text/" . ($this->_html ? 'html' : 'plain') . "; charset=" . $this->_charset . "\r\n";
                $body = $this->_body;
                break;
            case 'related':
                $headers.= "Content-type: multipart/related; boundary=\"PHP-related-$boundary\"\r\n";
                $body = $this->_generateRelatedBody($boundary);
                break;
            case 'alt':
                $headers.= "Content-type: multipart/alternative; boundary=\"PHP-alt-$boundary\"\r\n";
                $body = $this->_generateAlternativeBody($boundary);
                break;
            case 'mixed':
                $headers.= "Content-type: multipart/mixed; boundary=\"PHP-mixed-$boundary\"\r\n";
                $body = $this->_generateMixedBody($boundary);
                break;
        }

        if (count($this->_recipientsCc)) {
            foreach ($this->_recipientsCc AS $cc) {
                $headers.= "Cc: " . (isset($cc['name']) ? $cc['name'] . ' <' . $cc['addr'] . '>' : $cc['addr']) . "\r\n";
            }
        }

        if (count($this->_recipientsBcc)) {
            foreach ($this->_recipientsBcc AS $bcc) {
                $headers.= "Bcc: " . (isset($bcc['name']) ? $bcc['name'] . ' <' . $bcc['addr'] . '>' : $bcc['addr']) . "\r\n";
            }
        }

        if ($type == 'mixed')
            $headers .= "Content-transfer-encoding: 7BIT\r\n";
        $headers.= "X-Mailer: MailSender 1.0 with PHP/" . phpversion() . "\r\n\r\n";

        $toArray = array();
        foreach ($this->_recipients AS $to) {
            $toArray[] = isset($to['name']) ? $to['name'] . ' <' . $to['addr'] . '>' : $to['addr'];
        }
        $toString = implode(', ', $toArray);

        if (!mail($toString, $this->_subject, $body, $headers))
            throw new MailSenderException('Mail was not sent succesfully. Please check your errors');
    }

    /**
     * Generuje źródło wiadomości z załącznikami
     * @param string $boundary Separator części wiadomości
     * @return string
     */
    protected function _generateMixedBody($boundary) {

        if ($this->_html && $this->_alternativeBody) {

            $body = "--PHP-mixed-{$boundary}\n";
            $body.= "Content-Type: multipart/alternative; boundary=\"PHP-alt-{$boundary}\"\n\n";

            $body.= $this->_generateAlternativeBody($boundary);
        } elseif ($this->_html) {

            if (count($this->_attachmentsWithCid)) {
                $body.= "--PHP-mixed-{$boundary}\n";
                $body.= "Content-Type: multipart/related; boundary=\"PHP-related-{$boundary}\"\n\n";

                $body.= $this->_generateRelatedBody($boundary);
            } else {
                $body.= "--PHP-mixed-{$boundary}\n";
                $body.= "Content-Type: text/html; charset={$this->_charset};\n\n";

                $body.= "{$this->_body}\n";
            }
        } else {

            $body.= "--PHP-mixed-{$boundary}\n";
            $body.= "Content-Type: text/plain; charset={$this->_charset};\n\n";

            $body.= "{$this->_body}\n";
        }

        foreach ($this->_attachments AS $att) {
            $body .= $this->_getAttachmentString($att, $boundary);
        }

        $body .= "--PHP-mixed-" . $boundary . "--\n";

        return $body;
    }

    /**
     * Generuje źródło wiadomości z alternatywnymi treściami
     * @param string $boundary Separator części wiadomości
     * @return type
     */
    protected function _generateAlternativeBody($boundary) {

        $body = "--PHP-alt-{$boundary}\n";
        $body.= "Content-Type: text/plain; charset={$this->_charset};\n\n";

        $body.= "{$this->_alternativeBody}\n";

        if (count($this->_attachmentsWithCid)) {
            $body.= "--PHP-alt-{$boundary}\n";
            $body.= "Content-Type: multipart/related; boundary=\"PHP-related-{$boundary}\"\n\n";

            $body.= "--PHP-related-{$boundary}\n";
            $body.= "Content-Type: text/html; charset={$this->_charset};\n\n";

            $body.= "{$this->_body}\n";

            foreach ($this->_attachmentsWithCid AS $att) {
                $body .= $this->_getAttachmentWithCidString($att, $boundary);
            }

            $body.= "--PHP-related-{$boundary}--\n";
        } else {
            $body.= "--PHP-alt-{$boundary}\n";
            $body.= "Content-Type: text/html; charset={$this->_charset};\n\n";

            $body.= "{$this->_body}\n";
        }

        $body.= "--PHP-alt-{$boundary}--\n";

        return $body;
    }

    /**
     * Generuje źródło wiadomości z załącznikami typu inline
     * @param string $boundary Separator części wiadomości
     * @return type
     */
    protected function _generateRelatedBody($boundary) {
        $body.= "--PHP-related-{$boundary}\n";
        $body.= "Content-Type: text/html; charset={$this->_charset};\n\n";

        $body.= "{$this->_body}\n";

        foreach ($this->_attachmentsWithCid AS $att) {
            $body .= $this->_getAttachmentWithCidString($att, $boundary);
        }

        $body.= "--PHP-related-{$boundary}--\n";

        return $body;
    }

    /**
     * Zwraca kod załącznika
     * @param array $att Tablica danych załącznika
     * @param string $boundary Separator części wiadomości
     * @return string
     */
    protected function _getAttachmentString($att, $boundary) {
        return "--PHP-mixed-{$boundary}\n" .
                "Content-type: {$att['mime']}; name=\"{$att['filename']}\"\n" .
                "Content-Transfer-Encoding: base64\n" .
                "Content-disposition: attachment; filename=\"{$att['filename']}\"\n\n" .
                "{$att['content']}\n";
    }

    /**
     * Zwraca kod załącznika typu inline
     * @param array $att Tablica danych załącznika
     * @param string $boundary Separator części wiadomości
     * @return string
     */
    protected function _getAttachmentWithCidString($att, $boundary) {
        return "--PHP-related-{$boundary}\n" .
                "Content-type: {$att['mime']}; name=\"{$att['filename']}\"\n" .
                "Content-Transfer-Encoding: base64\n" .
                "Content-ID: <{$att['cid']}>\n\n" .
                "{$att['content']}\n";
    }

    /**
     * Zwraca typ mime na podstawie rozszerzenia pliku
     * @param string $filePath Ścieżka lub nazwa pliku
     * @return string
     */
    private function _getMimeType($filePath) {
        if (preg_match('#\.([a-z0-9]+)$#', $filePath, $match)) {
            $extension = $match[1];
            $mimeTypes = array(
                'ai' => 'application/postscript',
                'aif' => 'audio/x-aiff',
                'aifc' => 'audio/x-aiff',
                'aiff' => 'audio/x-aiff',
                'avi' => 'video/x-msvideo',
                'bin' => 'application/macbinary',
                'bmp' => 'image/bmp',
                'class' => 'application/octet-stream',
                'cpt' => 'application/mac-compactpro',
                'css' => 'text/css',
                'dcr' => 'application/x-director',
                'dir' => 'application/x-director',
                'dll' => 'application/octet-stream',
                'dms' => 'application/octet-stream',
                'doc' => 'application/msword',
                'dvi' => 'application/x-dvi',
                'dxr' => 'application/x-director',
                'eml' => 'message/rfc822',
                'eps' => 'application/postscript',
                'exe' => 'application/octet-stream',
                'gif' => 'image/gif',
                'gtar' => 'application/x-gtar',
                'htm' => 'text/html',
                'html' => 'text/html',
                'jpe' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'hqx' => 'application/mac-binhex40',
                'js' => 'application/x-javascript',
                'lha' => 'application/octet-stream',
                'log' => 'text/plain',
                'lzh' => 'application/octet-stream',
                'mid' => 'audio/midi',
                'midi' => 'audio/midi',
                'mif' => 'application/vnd.mif',
                'mov' => 'video/quicktime',
                'movie' => 'video/x-sgi-movie',
                'mp2' => 'audio/mpeg',
                'mp3' => 'audio/mpeg',
                'mpe' => 'video/mpeg',
                'mpeg' => 'video/mpeg',
                'mpg' => 'video/mpeg',
                'mpga' => 'audio/mpeg',
                'oda' => 'application/oda',
                'pdf' => 'application/pdf',
                'php' => 'application/x-httpd-php',
                'php3' => 'application/x-httpd-php',
                'php4' => 'application/x-httpd-php',
                'phps' => 'application/x-httpd-php-source',
                'phtml' => 'application/x-httpd-php',
                'png' => 'image/png',
                'ppt' => 'application/vnd.ms-powerpoint',
                'ps' => 'application/postscript',
                'psd' => 'application/octet-stream',
                'qt' => 'video/quicktime',
                'ra' => 'audio/x-realaudio',
                'ram' => 'audio/x-pn-realaudio',
                'rm' => 'audio/x-pn-realaudio',
                'rpm' => 'audio/x-pn-realaudio-plugin',
                'rtf' => 'text/rtf',
                'rtx' => 'text/richtext',
                'rv' => 'video/vnd.rn-realvideo',
                'sea' => 'application/octet-stream',
                'shtml' => 'text/html',
                'sit' => 'application/x-stuffit',
                'so' => 'application/octet-stream',
                'smi' => 'application/smil',
                'smil' => 'application/smil',
                'swf' => 'application/x-shockwave-flash',
                'tar' => 'application/x-tar',
                'text' => 'text/plain',
                'txt' => 'text/plain',
                'tgz' => 'application/x-tar',
                'tif' => 'image/tiff',
                'tiff' => 'image/tiff',
                'wav' => 'audio/x-wav',
                'wbxml' => 'application/vnd.wap.wbxml',
                'wmlc' => 'application/vnd.wap.wmlc',
                'word' => 'application/msword',
                'xht' => 'application/xhtml+xml',
                'xhtml' => 'application/xhtml+xml',
                'xl' => 'application/excel',
                'xls' => 'application/vnd.ms-excel',
                'xml' => 'text/xml',
                'xsl' => 'text/xml',
                'zip' => 'application/zip'
            );
            if (isset($mimeTypes[$extension]))
                return $mimeTypes[$extension];
        }
        return 'application/octet-stream';
    }

    /**
     * Stosuje kodowanie znaków na podanym ciągu
     * @param string $text
     * @return string
     */
    protected function _useCharset($text) {
        return '=?' . strtoupper($this->_charset) . '?B?' . base64_encode($text) . '?=';
    }

}

/**
 * Klasa wyjątku klasy MailSender
 */
class MailSenderException extends Exception {

}