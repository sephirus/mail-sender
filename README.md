# Mail Sender

Klasa MailSender pozwala na proste wysyłanie wiadomości e-mail ze strony opartej na PHP 5.3+. Klasa oparta jest na funkcji [mail()][1], której używa do samego wysyłania wiadomości. Cała obudowa tej funkcji pozwala na wygodne i proste wykorzystywanie tej funkcji łącznie z wysyłaniem załączników.

## Dla kogo jest ta klasa

Klasa skierowana jest dla początkujących programistów, którzy mają problemy z wysyłaniem wiadomości e-mail z poziomu PHP. Jest ona nakładką na funkcję [mail()][1], która sama w sobie jest niewygodna w użyciu jeżeli istnieje potrzeba wysłania czegoś więcej niż prostej wiadomości.
Jeśli masz problemy z kodowaniem znaków wiadomości w jej temacie bądź treści lub potrzebujesz łatwego narzędzia do wysyłania wiadmości z załącznikami lub grafikami zawartymi w jej treści to dobrze trafiłeś.

### Zalety

Ta prosta klasa pozwala na:

 - wysyłanie wiadmości e-mail w formie tekstowej i HTML,
 - wysyłanie załączników,
 - łatwe wysyłanie wiadomości z grafikami zawartami w jej treści,
 - stosowanie odpowiedniego kodowania podczas wysyłania wiadomości.

## Wymagania

Klasa do poprawnego działania wymaga PHP w wersji conajmniej 5.3 oraz skonfigurowanej na serwerze funkcji [mail()][1].

## Instrukcja i przykłady użycia

Poniżej przedstawiam kilka prostych przykładów dzięki, którym w pełni poznasz możliwości klasy MailSender.

### Wysyłanie prostej wiadomości tekstowej

Najprostsza wiadomość e-mail wysłana za pośrednictwem klasy MailSender jest zbliżona do kodu poniżej.

    :::php
    $ms = new MailSender();
    $ms->setFrom('moj_adres@jakas_domena.pl'); // Nadawca
    $ms->setRecipient('adres_odbiorcy@jakas_domena.pl'); // Odbiorca
    $ms->setSubject('To jest tytuł wiadomości');
    $ms->setBody('To jest treść wiadomości');
    $ms->send();

Należy pamiętać, że metoda `send` nie przyjmuje żadnych argumentów.

### Określanie odbiorców, odbiorców kopii oraz odbiorców kopii ukrytej

Istnieje mozliwość określenia każdego z trzech typów odbiorców wiadomości.

    :::php
    $ms = new MailSender();
    $ms->setFrom('moj_adres@jakas_domena.pl'); // nadawca
    $ms->setRecipient('adres_odbiorcy@jakas_domena.pl'); // odbiorca
    $ms->setRecipientCc('odbiorca_kopii@jakas_domena.pl'); // odbiorca kopii
    $ms->setRecipientBcc('odbiorca_kopii_ukrytej@jakas_domena.pl'); // odbiorca kopii ukrytej
    $ms->setReplyTo('moj_inny_adres_zwrotny@jakas_domena.pl'); // domyślnie jest to ten sam adres co nadawcy
    $ms->setSubject('To jest tytuł wiadomości');
    $ms->setBody('To jest treść wiadomości');
    $ms->send();

Aby dodać więcej niż jednego odbiorcę zamiast metod `setRecipient`, `setRecipientCc`, `setRecipientBcc` należy użyć odpowiednio metod `addRecipient`, `addRecipientCc`, `addRecipientBcc`.

    :::php
    $ms = new MailSender();
    $ms->setFrom('moj_adres@jakas_domena.pl');
    // odbiorcy:
    $ms->addRecipient('adres_odbiorcy1@jakas_domena.pl'); 
    $ms->addRecipient('adres_odbiorcy2@jakas_domena.pl'); 
    // odbiorcy kopii:
    $ms->addRecipientCc('odbiorca_kopii1@jakas_domena.pl'); 
    $ms->addRecipientCc('odbiorca_kopii2@jakas_domena.pl'); 
    $ms->addRecipientCc('odbiorca_kopii3@jakas_domena.pl'); 

    $ms->setReplyTo('moj_inny_adres_zwrotny@jakas_domena.pl'); 
    $ms->setSubject('To jest tytuł wiadomości');
    $ms->setBody('To jest treść wiadomości');
    $ms->send();

Każda z metod `setFrom`, `setRecipient`, `setRecipientCc`, `setRecipientBcc`, `addRecipient`, `addRecipientCc`, `addRecipientBcc` przyjmuje dodatkowy argument, który pozwala na podanie nazwy nadawcy bądź odbiorcy.

#### Przykład z odbiorcą i nadawcą
    :::php
    $ms = new MailSender();
    $ms->setFrom('moj_adres@jakas_domena.pl','Mariusz Fornal'); // Nadawca (Mariusz Fornal <moj_adres@jakas_domena.pl>)
    $ms->setRecipient('adres_odbiorcy@jakas_domena.pl','Karol Wielki'); // Odbiorca (Karol wielki <adres_odbiorcy@jakas_domena.pl>)
    $ms->setSubject('To jest tytuł wiadomości');
    $ms->setBody('To jest treść wiadomości');
    $ms->send();

### Kodowanie znaków wiadomości

W trybie domyślnym klasa MailSender pracuje na kodowaniu UTF-8 i wszsytkie przekazywane jej treści (nazwy odbiorców i nadawców, temat, treść) muszą być jej przekazywane w tym kodowaniu. Istnieje jednak możliwość określenia innego kodowania. Ze względu na fakt, że obiekt wykonuje operacja przygotowania treści przed ich wysłaniem kodowanie znaków należy określić w konstruktorze klasy.

    :::php
    $ms = new MailSender('iso-8859-2');
    // dalszy kod...

### Wysyłanie wiadomości jako kod HTML

W trybie domyślnym wiadomości są wysyłane jako zwykły tekst. Aby wysłać wiadomość jako kod HTML wystarczy użyć metody `isHTML` i jako treść podać kod HTML.

    :::php
    $ms = new MailSender();
    $ms->isHTML(true);
    $ms->setBody('<b>To jest treść HTML</b>');
    // dalszy kod...

#### Ustawianie tekstu alternatywnego

Niektóre z aplikacji do odbioru wiadomości e-mail mogą nie obdługiwać kodu HTML. Należy wówczas podawać także tekst alternatywny pozbawiony znaczników. Służy do tego metoda `setAlternativeBody`.

    :::php
    $ms = new MailSender();
    $ms->isHTML(true);
    $ms->setBody('<b>To jest treść HTML</b>');
    $ms->setAlternativeBody('To jest treść tekstowa, która pokaże się tam gdzie HTML jest nieobsługiwany');
    // dalszy kod...

### Dodawanie załączników

Załączniki można dodawać na dwa sposoby.

#### Załączniki z zewnętrznych plików

Wystarczy użyć metody `addAttachmentFromFile` podając w jej argumencie ścieżkę do pliku jaki ma zostać wysłany jako załącznik.

    :::php
    $ms = new MailSender();
    // Ustawianie odbiorców, treści itp.
    $ms->addAttachmentFromFile('/sciezka/do/pliku1.zip');
    $ms->addAttachmentFromFile('/sciezka/do/pliku2.zip');
    $ms->send();

#### Załączniki z treści PHP

W przypadku gdy na przykład chcemy wysłać obrazek, który jest zapisany w bazie danych możemy posłużyć się metodą 

    :::php
    $image; // zmienna pobrana z bazy przechowująca treść obrazka

    $ms = new MailSender();
    // Ustawianie odbiorców, treści itp.
    $ms->addAttachment($image, 'obrazek.png', 'image/png');
    $ms->send();

Metoda `addAttachment` przyjmuje trzy argumenty:

 - treść pliku,
 - nazwę pliku, która pokaże się w wiadomości jako nazwa załącznika,
 - typ mime pliku.

Wszystkie argumenty są wymagane.

### Dodawanie grafik inline

Obie metody `addAttachment` oraz `addAttachmentFromFile` przyjmują jeden dodatkowy parametr. Jest to identyfikator załącznika inline. Aby wyświetlić obrazek z załącznika wiadomości w jej treści HTML można posłużyć się następującym przykładem.

    :::php
    $ms = new MailSender();
    // Ustawianie odbiorców, treści itp.
    $ms->setBody('To jest obrazek: <img src="cid:obrazek1"/><br/><br/>i to też: <img src="cid:obrazek2"/>');
    $ms->addAttachmentFromFile('/sciezka/do/obr1.png','obrazek1');
    $ms->addAttachmentFromFile('/sciezka/do/obr2.png','obrazek2');
    $ms->send();

Tak wysłana wiadomość nie będzie posiadała widocznych załączników a te dodane w ten sposób zostaną wyświetlone w treści wiadomości.

## Powodzenia

Mam nadzieję, że znajdziesz zastosowanie dla tej klasy. W razie jakichś sugestii, błędów lub pytań pisz śmiało.

[1]: http://php.net/manual/en/function.mail.php