# Mail Sender - zmiany

## Wersja 1.0.4

### Poprawki błędów

Poprawa błędu przy określaniu typu Mime pliku.

### Poprawa kompatybilności ze starszymi wersjami PHP

Usunięto funkcję uniemożliwiającą działanie na starszych wersjach PHP

## Wersja 1.0.3

### Własne nazwy załączników

Dodanie możliwości podania własnej nazwy pliku przy dodawaniu załącznika z zewnętrznego pliku (metoda `addAttachmentFromFile`).

    :::php
    $ms = new MailSender;
    $ms->addAttachmentFromFile('sciezka/do/pliku.jpg', null, 'wakacje_w_lebie.jpg');
    // dalszy kod

### Tablica typów MIME

Dzięki tablicy typów MIME dodanej do klasy, argument `$mimeType` w metodzie `addAttachment` staje się opcjonalny.

- - -

## Wersja 1.0

Podstawowa wersja klasy.